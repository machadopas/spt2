# RESPOSTAS ESTUDO DIRIGIDO

1. É um sistema de controle de versão, é usado para desenvolver projetos em colaboração, criando e editando arquivos sem que alterações sejam sobrescritas
2. Local onde ficam as modificações até o próximo commit
3. Árvore de diretórios dos arquivos
4. Guardar as modificações
5. Ramificação do código
6. Ponteiro pro branch atual
7. Junção de dois branch
8. - untracked: arquivo que não estava no último commit
   - unmodified: arquivo não modificado
   - staged: arquivos modificados
   arquivos prontos pro commit
9. Cria um repositório
10. Adiciona arquivo ao repositório
11. Verificar status dos arquivos do repositório
12. Salva no git as alterações
13. Mostra histórico de commit
14. Cria um novo branch
15. Resets movem HEAD para o commit indicado, mantendo ou alteando os staging e working directory 
    | NADA  | STAGING | WORKING |
    | ----- | ------- | ------- |
    | SOFT  | MANTEM  | MANTEM  |
    | MIXED | ALTERA  | MANTEM  |
    | HARD  | ALTERA  | ALTERA  |
16. cria novo commit com alterações do commit indicado
17. Copia um repositório
18. Envia alterações para repositório remote
19. Recebe alterações de um repositório remoto
20. Criando arquivo .gitignore e listando os arquivos
21. - Main: branch principal
    - Develop: branch que recebe as modificações de correção ou evolução
    - Staging: branch para testes das modificações